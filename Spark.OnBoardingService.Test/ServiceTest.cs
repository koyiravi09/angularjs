﻿using Microsoft.AspNetCore.Mvc;
using Spark.OnBoardingServices;
using Spark.OnBoardingServices.Controllers;
using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Xunit;

namespace Spark.OnBoardingService.Test
{
    public class ServiceTest
    {
        ServiceController serviceController;
        public ServiceTest()
        {
            serviceController = new ServiceController();
        }

        [Fact]
        public async void InsertService()
        {
            Service service = new Service()
            {
             
                serviceName = "Cognito",
                serviceDescription = "It provides users authentication and authorization service",
                isActive = 1,
                userId = 11,
                processType = "insert"
            };

            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        [Fact]
        public async void UpdateService()
        {
            Service service = new Service()
            {
                serviceId = 2,
                serviceName = "storage",
                serviceDescription = "It provides users storage",
                isActive = 0,
                processType = "update"
            };
            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
            [Fact]
            public async void serviceDelete()
            {
            Service service = new Service()
            {
                serviceId = 1,
                processType = "delete"
            };
            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
            }

            [Fact]
            public async void GetServiceById()
            {
            Service service = new Service()
            {
                serviceId = 1,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

            [Fact]
            public async void GetserviceByLimtit()
            {
            Service service = new Service()
            {
                startIndex = 1,
                limit = 20,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);

            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

            [Fact]
            public async void GetService()
            {
            Service service = new Service()
            {
                processType = "get"
            };

            var jsonElement = GetJsonElement<Service>(service);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void InsertRoleService()
        {
           Role role = new Role()
            {
             
               roleName = "developer",
               roleDescription = "developing a product",
               serviceId = 2,
               isActive  = true,
               userId = 2,
               processType = "insertRoleService"
            };

            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateRoleService()
        {
            Role role = new Role()
            {
                roleId = 1,
                roleName = "developer",
                roleDescription = "developing a product",
                serviceId = 2,
                isActive = true,
                processType = "updateRoleService"
            };

            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        [Fact]
        public async void DeleteRoleService()
        {
            Role role = new Role()
            {
                roleId = 1,
                serviceId = 1,
                processType = "deleteRoleService"
            };

            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        [Fact]
        public async void GetRoleServiceById()
        {
            Role role = new Role()
            {
                serviceId = 1,
                processType = "getRoleServiceById"
            };

            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetRoleserviceByLimit()
        {
            Role role = new Role()
            {
                startIndex = 1,
                limit = 20,
                processType = "getRoleserviceByServiceId"
            };

            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetRoleService()
        {
            Role role = new Role()
            {
                roleId =2,
                serviceId = 1,
                processType = "getRoleService"
            };
            var jsonElement = GetJsonElement<Role>(role);
            // Act
            var OkResult = await serviceController.RegisterService(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        private JsonElement GetJsonElement<T>(T obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            var jsonDoc = JsonDocument.Parse(json);
            var jsonElement = jsonDoc.RootElement.Clone();

            return jsonElement;
        }


    }
}

