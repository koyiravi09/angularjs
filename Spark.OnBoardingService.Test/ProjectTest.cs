﻿using Microsoft.AspNetCore.Mvc;
using Spark.OnBoardingServices.Controllers;
using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Xunit;

namespace Spark.OnBoardingService.Test
{
    public class ProjectTest
    {
        ProjectController projectController;
        public ProjectTest()
        {
            projectController = new ProjectController();
        }

        [Fact]
        public async void InsertProject()
        {
            Project project = new Project()
            {
               
                clientId = 1,
                projectName = ".Net",
                projectDescription = ".Net Core projects",
                projectStatusId = 1,
                processType = "insert"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateProject()
        {
            Project project = new Project()
            {
                clientProjectId = 1,
                clientId = 2,
                projectName = "java",
                projectDescription = ".Net Core Services and procedure",
                projectStatusId = 1,
                processType = "update"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DeleteProject()
        {
            Project project = new Project()
            {
                clientProjectId = 2,
                processType = "delete"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetProjectById()
        {
            Project project = new Project()
            {
                clientProjectId = 1,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetProjectByLimit()
        {
            Project project = new Project()
            {
                startIndex = 2,
                limit = 14,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetProject()
        {
            Project project = new Project()
            {
              processType = "get"
            };
            var jsonElement = GetJsonElement<Project>(project);
            // Act
            var OkResult = await projectController.RegisterProject(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }
        private JsonElement GetJsonElement<T>(T obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            var jsonDoc = JsonDocument.Parse(json);
            var jsonElement = jsonDoc.RootElement.Clone();

            return jsonElement;
        }

    }
}
