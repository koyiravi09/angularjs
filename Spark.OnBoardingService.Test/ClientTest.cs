using Microsoft.AspNetCore.Mvc;
using Spark.OnBoardingServices;
using Spark.OnBoardingServices.Controllers;
using Spark.OnBoardingServices.Models;
using System;
using System.Text.Json;
using Xunit;

namespace Spark.OnBoardingService.Test
{
    public class ClientTest
    {
        ClientController clientController ;
        public ClientTest()
        { 
            clientController = new ClientController();
        }

        [Fact]
        public async void InsertClient()
        {
            Client client = new Client()
            {
                clientName = "Jay",
                clientDescription = "man",
                addressLine_1 = "Chennai",
                addressLine_2 = "Ennore",
                postalCode = "600057",
                stateProvinceCountry = "TamilNadu",
                country = "India",
                contactNumber = "7358224169",
                isActive = true,
                processType = "insert",
                userId = 1
            };
           
                var jsonElement = GetJsonElement<Client>(client);
                // Act
                var OkResult = await clientController.RegisterClient(jsonElement);
                // Assert
                Assert.IsType<ActionResult<dynamic>>(OkResult);
         
        }

        [Fact]
        public async void UpdateClient()
        {
            Client client = new Client()
            {
                userId = 1,
                clientName = "Jay",
                clientDescription = "man",
                addressLine_1 = "Chennai",
                addressLine_2 = "Ennore",
                postalCode = "600057",
                stateProvinceCountry = "TamilNadu",
                country = "India",
                contactNumber = "7358224169",
                isActive = true,
                processType = "update",
            };

            var jsonElement = GetJsonElement<Client>(client);

            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DelteClient()
        {
            Client client = new Client()
            {
                clientId = 1,
                processType = "delete"
            };
            var jsonElement = GetJsonElement<Client>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientById()
        {
            Client client = new Client()
            {
                clientId = 1,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Client>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientByLimtit()
        {
            Client client = new Client()
            {
                startIndex = 1,
                limit = 20,
                processType = "get"
            };
            var jsonElement = GetJsonElement<Client>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetAllClient()
        {
            Client client = new Client()
            {
                processType = "get"
            };
            var jsonElement = GetJsonElement<Client>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void InsertClientService()
        {
            ClientService client = new ClientService()
            {
                userId = 1,
                clientName = "Jay",
                clientDescription = "man",
                serviceId = 2,
                serviceName = "AWS",
                serviceDescription = "storage",
                processType = "insertClientService"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void UpdateClientService()
        {
            ClientService client = new ClientService()
            {
                userId = 1,
                serviceId = 2,
                processType = "updateClientService"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void DeleteClientService()
        {
            ClientService client = new ClientService()
            {
                userId=1,
                clientId = 1,
                serviceId = 2,
                processType = "deleteClientService"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientServiceByClientId()
        {
            ClientService client = new ClientService()
            {
                clientId = 1,
                processType = "getClientServiceByClientId"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientServiceByServiceId()
        {
            ClientService client = new ClientService()
            {
                serviceId = 2,
                processType = "getClientServiceByServiceId"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }

        [Fact]
        public async void GetClientService()
        {
            ClientService client = new ClientService()
            {
                clientId = 1,
                serviceId = 2,
                processType = "getClientService"
            };
            var jsonElement = GetJsonElement<ClientService>(client);
            // Act
            var OkResult = await clientController.RegisterClient(jsonElement);
            // Assert
            Assert.IsType<ActionResult<dynamic>>(OkResult);
        }


        private JsonElement GetJsonElement<T>(T obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            var jsonDoc = JsonDocument.Parse(json);
            var jsonElement = jsonDoc.RootElement.Clone();

            return jsonElement;
        }
    }
}
