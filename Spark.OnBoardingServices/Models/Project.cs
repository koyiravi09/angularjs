﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Spark.OnBoardingServices.Models
{
    public class Project : Pagination
    {
        public Int32? clientProjectId { get; set; }
        public Int32? clientId { get; set; }
        public Int32? userId { get; set; }
        public string projectName { get; set; }
        public string projectDescription { get; set; }
        public Int32? projectStatusId { get; set; }
        public string service { get; set; }
        public DateTime? lastProjectUpdatedDateTime { get; set; }
        public DateTime? lastClientUpdatedDateTime { get; set; }
        public string clientName { get; set; }
    }
}