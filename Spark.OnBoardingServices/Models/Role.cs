﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class Role : Pagination
    {
        public Int32? roleId { get; set; }
        public string roleName { get; set; }
        public string serviceName { get; set; }
        public string roleDescription { get; set; }
        public Int32? serviceId { get; set; }
        public Int32? userId{ get; set; }
        public bool isActive { get; set; }
    }
}
