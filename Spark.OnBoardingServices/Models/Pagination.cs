﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class Pagination
    {
        public string  processType { get; set; }
        public Int32? startIndex { get; set; }
        public Int32? limit { get; set; } = 10;
    }
}
