﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices
{
    public class Client : Pagination
    {
        public Int32? clientId { get; set; }
        public Int32? userId { get; set; }
        public string clientName { get; set; }
        public string clientDescription { get; set; }
        public string addressLine_1 { get; set; }
        public string addressLine_2 { get; set; }
        public string postalCode { get; set; }
        public string stateProvinceCountry { get; set; }  
        public string country { get; set; }
        public string contactNumber { get; set; }
        public string bucketName { get; set; }
        public bool isActive { get; set; } 
    }
}
