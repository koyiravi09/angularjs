﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Models
{
    public class ClientService : Pagination
    {
        public Int32? clientServiceMappingId { get; set; }
        public Int32? clientId { get; set; }
        public Int32? serviceId { get; set; }
        public Int32? userId { get; set; }
        public string clientName { get; set; }
        public string serviceName { get; set; }
        public string clientDescription { get; set; }
        public string serviceDescription { get; set; }
    }
}
