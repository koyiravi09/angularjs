﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Spark.OnBoardingServices
{
    public  class Connectors
    {
       public string connectionString = null;
       public Connectors()
       {
            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(),"appsettings.json"));
            var root = builder.Build();
            connectionString = root.GetSection("ConnectionString").GetSection("connectionString").Value;    
       }
    }
}
