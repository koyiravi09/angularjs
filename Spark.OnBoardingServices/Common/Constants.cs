﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Common
{
    public static class AppConstants
    {
        public const string Insert = "insert";
        public const string Update = "update";
        public const string Delete = "delete";
        public const string Get = "get";
        public const string GetRoles = "getRoles";
        public const string InsertClientService = "insertClientService";
        public const string UpdateClientService = "updateClientService";
        public const string DeleteClientService = "deleteClientService";
        public const string GetClientService = "getClientService";
        public const string GetClientServiceByClientId = "getClientServiceByClientId";
        public const string GetClientServiceByServiceId = "getClientServiceByServiceId";
        public const string InsertClientUser = "insertClientUser";
        public const string UpdateClientUser = "updateClientUser";
        public const string DeleteClientUser = "deleteClientUser";
        public const string GetClientUser = "getClientUser";
        public const string GetClientUserByClientId = "getClientUserByClientId";
        public const string GetClientUserByUserId = "getClientUserByUserId";
        public const string InsertUserRole = "insertUserRole";
        public const string UpdateUserRole = "updateUserRole";
        public const string DeleteUserRole = "deleteUserRole";
        public const string GetUserRole = "getUserRole";
        public const string GetUserRoleByUserId = "getUserRoleByUserId";
        public const string GetUserRoleByRoleIds = "getUserRoleByRoleIds";
        public const string GetUserRoleByRoleId = "getUserRoleByRoleId";
        public const string GetUserRoleByClientServiceRoleId = "getUserRoleByClientServiceRoleId";
        public const string InsertRoleService = "insertRoleService";
        public const string UpdateRoleService = "updateRoleService";
        public const string DeleteRoleService = "deleteRoleService";
        public const string GetRoleService = "getRoleService";
        public const string GetRoleServiceByRoleId = "getRoleServiceByRoleId";
        public const string GetRoleServiceByServiceId = "getRoleServiceByServiceId";
        public const string ProjectStatus = "projectStatus";
        public const string InsertProjectService = "insertProjectService";
        public const string UpdateProjectService = "updateProjectService";
        public const string DeleteProjectService = "deleteProjectService";
        public const string GetProjectServiceByProjectId = "getProjectServiceByProjectId";
        public const string GetProjectService = "getProjectService";
        public const string ErrorMessage = "Something went wrong. Please contact administrator";
        public const string InsertComplianceRequirement = "insertComplianceRequirement";
        public const string InsertTranslationRequirement = "insertTranslationRequirement";
        public const string InsertStorageArchivalRequirement = "insertStorageArchivalRequirement";
        public const string UpdateComplianceRequirement = "updateComplianceRequirement";
        public const string UpdateTranslationRequirement = "updateTranslationRequirement";
        public const string UpdateStorageArchivalRequirement = "updateStorageArchivalRequirement";
        public const string GetProjectOptions = "getProjectOptions";
        public const string AlreadyExist = "alreadyExist";
        public const string Bucketvalidation = "bucketValidation";
        public const string BucketCreated = "bucket created successfully";
        public const string BucketExists = "bucket name already exist";
        public const string FolderCreated = "folder created successfully";
        public const string FolderExist = "folder already exist";
        public const string loginFailed = "Invalid Username/Password";
       
        public const int Success = 200;
        public const int NoContent = 204;
        public const int Created = 201;
        public const int InternalServerError = 500;
        public const int BadRequest = 400;
        public const int NotFound = 404;
        public const int ValidationError = 212; 
    }
}
