﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.Extensions.Configuration;
using Spark.OnBoardingServices.Models;
using Spark.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Common
{
    public static class CommonUtil
    {
        static Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.APSouth1;
        
        public static ResponseModel ResponseObject(Int32? id, Int32? flag)
        {
            try
            {
                ResponseModel response = new ResponseModel();
                string responseMessage = "";

                if (flag == 1)
                {
                    responseMessage = "Inserted successfully";
                }
                else if (flag == 2)
                {
                    responseMessage = "Updated Successfully";
                }
                else if (flag == 3)
                {
                    responseMessage = "Deleted successsfully";
                }

                response.id = id;
                response.message = responseMessage;
                return response;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<dynamic> GetUserDetailsAsync(dynamic user)
        {

            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), Constants.appSettingJson));
            var root = builder.Build();
            var password = root.GetSection("Password").Value;
            string phoneNumber = " +911627577890";
            string appClienId = "7l4h94jdkdds6er19a1jmj1am1";
            string awsAccessKeyId = "AKIAXMZKJKCMM262FPFO";
            string awsSecretAccessKey = "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";
            try {
                AmazonCognitoIdentityProviderClient provider =
                 new AmazonCognitoIdentityProviderClient(awsAccessKeyId, awsSecretAccessKey, Region);
                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = appClienId,
                    Username = user.emailId,
                    Password = password
                };
                List<AttributeType> attributes = new List<AttributeType>()
                {
                new AttributeType(){ Name="email",Value = user.emailId},
                new AttributeType(){ Name="gender",Value = user.gender},
                new AttributeType(){ Name="given_name",Value = user.displayName},
                new AttributeType(){ Name="name",Value=user.firstName},
                new AttributeType(){ Name="phone_number",Value = phoneNumber},
                 };
                signUpRequest.UserAttributes = attributes;
                SignUpResponse response = await provider.SignUpAsync(signUpRequest);
                return "Success";

            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return ex.Message;
            }
        }
    }
}
