﻿using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.Transactions
{
    public class UnitOfWork : Connectors
    {
        private SqlConnection connection = null;
        private SqlTransaction transaction = null;
        private IUser userData = new UserData();
        private IClient clientData = new ClientData();
        private IService serviceData = new ServiceData();
        private IProject projectData = new ProjectData();

        public UnitOfWork()
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
            transaction = connection.BeginTransaction();
        }

        public async Task<ResponseModel> userDelete(dynamic deleteObject)
        {
                await userData.DeleteUser(deleteObject.userId, deleteObject.logInUserId, connection, transaction);
                await userData.DeleteClientUserByUserId(deleteObject.userId, deleteObject.logInUserId, connection, transaction);
                await userData.DeleteUserRoleByUserId(deleteObject.userId, deleteObject.logInUserId, connection, transaction);
                Commit();
                return CommonUtil.ResponseObject(null, 3);
        }

        public async Task<ResponseModel> clientDelete(dynamic deleteObject)
        {
            try
            {
                await clientData.DeleteClient(deleteObject.clientId, deleteObject.userId, connection, transaction);
                await userData.DeleteClientUserByClientId(deleteObject.clientId, deleteObject.userId, connection, transaction);
                await clientData.DeleteClientServiceByClientId(deleteObject.clientId, deleteObject.userId, connection, transaction);
                Commit();
                return CommonUtil.ResponseObject(null, 3);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<ResponseModel> serviceDelete(dynamic deleteObject)
        {
            await serviceData.DeleteService(deleteObject.serviceId, deleteObject.userId, connection, transaction);
            await serviceData.DeleteRoleServiceByServiceId(deleteObject.serviceId, deleteObject.userId, connection, transaction);
            await serviceData.DeleteClientServiceByServiceId(deleteObject.serviceId,deleteObject.userId, connection, transaction);
            await projectData.DeleteProjectServiceByServiceId(deleteObject.serviceId, deleteObject.userId, connection, transaction);
            Commit();
            return CommonUtil.ResponseObject(null, 3);
        }

        public async Task<ResponseModel> projectDelete(dynamic deleteObject)
        {
            await projectData.DeleteProject(deleteObject.clientProjectId, deleteObject.userId, connection, transaction);
            await projectData.DeleteProjectServiceByProjectId(deleteObject.clientProjectId, deleteObject.userId, connection, transaction);
            Commit();
            return CommonUtil.ResponseObject(null, 3);
        }

        public void Commit()
        {
            try
            {
                transaction.Commit();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw;
            }
            finally
            {
                connection.Close();
            }

        }
    }
}
