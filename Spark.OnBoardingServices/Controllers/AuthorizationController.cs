﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    /// <summary>
    /// provide register and signin functionalities.
    /// </summary>
   
    [Route("api/authorization")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly ILogger<AuthorizationController> _logger;

        string poolId = "ap-south-1_DghaoxRzD";
        string appClienId = "7l4h94jdkdds6er19a1jmj1am1";
        static Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.APSouth1;
        string awsAccessKeyId = "AKIAXMZKJKCMM262FPFO";
        string awsSecretAccessKey = "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";
        private IUser userData = new UserData();

        /// <summary>
        /// passes a logger as a parameter.
        /// </summary>
        /// <param name="logger"></param>
        public AuthorizationController(ILogger<AuthorizationController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// register a user account information.
        /// </summary>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<ActionResult<string>> Register([FromBody] dynamic user)
        {
            try
            {
                AmazonCognitoIdentityProviderClient provider =
                 new AmazonCognitoIdentityProviderClient(awsAccessKeyId, awsSecretAccessKey, Region);
                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = appClienId,
                    Username = user.userName,
                    Password = user.password
                };
                List<AttributeType> attributes = new List<AttributeType>()
                {
                new AttributeType(){ Name="email",Value = user.emailId},
                new AttributeType(){ Name="gender",Value = user.gender},
                new AttributeType(){ Name="given_name",Value = user.displayName},
                new AttributeType(){ Name="name",Value=user.firstName},
                new AttributeType(){ Name="phone_number",Value = user.phoneNumber},
                 };
                signUpRequest.UserAttributes = attributes;

                SignUpResponse response = await provider.SignUpAsync(signUpRequest);
                return "Suceess";
            }
            catch(Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return ex.Message;
            }
        }

        /// <summary>
        /// provide username and password for returns a token string.
        /// </summary>
        /// <returns></returns>
        [HttpPost("signin")]
        public async Task<ActionResult<dynamic>> SignIn([FromBody] Login login)
        {
            //string username = "attomjay121998@gmail.com";
            //string password = "jayanth24";
           //  private IUser userData = new UserData();
             string userName = login.userName;
            string password = login.password;
            dynamic errorInfo = new ExpandoObject();
            try
            {
                var authReq = new AdminInitiateAuthRequest
                {
                    UserPoolId = poolId,
                    ClientId = appClienId,
                    AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH
                };
                authReq.AuthParameters.Add("USERNAME", userName);
                authReq.AuthParameters.Add("PASSWORD", password);
                AmazonCognitoIdentityProviderClient cognito =
                new AmazonCognitoIdentityProviderClient(awsAccessKeyId, awsSecretAccessKey, Region);
                AdminInitiateAuthResponse response = await cognito.AdminInitiateAuthAsync(authReq);

                dynamic userInfo = new ExpandoObject();

            
                userInfo.token = response.AuthenticationResult.IdToken;
                var userDetails = await userData.GetUserByEmailId(userName);

                if(userDetails!= null)
                {
                    userInfo.userDetails = userDetails;
                }
                else
                {
                    errorInfo.message = AppConstants.loginFailed;
                    return StatusCode(AppConstants.BadRequest, errorInfo);
                }

                return StatusCode(AppConstants.Success, userInfo);  
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.BadRequest, errorInfo);
            }
        }
      
    }
}
