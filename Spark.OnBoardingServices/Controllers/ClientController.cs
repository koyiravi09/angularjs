﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.OnBoardingServices.Transactions;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    [Route("api/psonboardingservices")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private IClient clientData = new ClientData();
        private AWSServices awsService = new AWSServices();
        private Object clientObject;

        [HttpPost("registerclient")]
        public async Task<ActionResult<dynamic>> RegisterClient([FromBody] JsonElement client)
        {
            dynamic finalResponse = null;
            try
            {
                var clientObject = getObject(client);

                if (clientObject is Client)
                {
                    Client clientInfo = (Client)clientObject;
                    if (clientInfo.processType == AppConstants.Insert)
                    {
                        finalResponse = await clientData.InsertClient(clientInfo);
                        await awsService.CreateBucket(clientInfo.bucketName);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (clientInfo.processType == AppConstants.Update)
                    {
                        finalResponse = await clientData.UpdateClient(clientInfo);                       
                            return StatusCode(AppConstants.Success, finalResponse);
                        
                    }
                    else if (clientInfo.processType == AppConstants.Delete)
                    {
                        UnitOfWork unitofwork = new UnitOfWork();
                        finalResponse = await unitofwork.clientDelete(clientInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (clientInfo.processType == AppConstants.Get)
                    {
                        if (clientInfo.clientId.HasValue)
                        {
                            finalResponse = await clientData.GetClient(clientInfo.clientId);
                            if (finalResponse != null)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else if (clientInfo.startIndex.HasValue)
                        {
                            finalResponse = await clientData.GetClient(clientInfo.startIndex, clientInfo.limit);
                            if(finalResponse !=null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else
                        {
                            finalResponse = await clientData.GetClient();
                            if(finalResponse != null && finalResponse.Count >0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                    }
                }
                else if (clientObject is ClientService)
                {
                    var clientService = (ClientService)clientObject;

                    if (clientService.processType == AppConstants.InsertClientService)
                    {
                        var result = await clientData.GetClientService(clientService);
                        if (result != null)
                        {
                            dynamic existMessage = new ExpandoObject();
                            existMessage.message = AppConstants.AlreadyExist;
                            return StatusCode(AppConstants.BadRequest, existMessage);
                        }
                        finalResponse = await clientData.InsertClientService(clientService);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (clientService.processType == AppConstants.UpdateClientService)
                    {
                        finalResponse = await clientData.UpdateClientService(clientService);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (clientService.processType == AppConstants.DeleteClientService)
                    {
                        finalResponse = await clientData.DeleteClientService(clientService,null,null);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (clientService.processType == AppConstants.GetClientServiceByClientId)
                    {
                        finalResponse = await clientData.GetClientServiceByClientId(clientService.clientId);
                        if(finalResponse!=null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }                     
                    }
                    else if (clientService.processType == AppConstants.GetClientServiceByServiceId)
                    {
                        finalResponse = await clientData.GetClientServiceByServiceId(clientService.serviceId);
                        if(finalResponse!=null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (clientService.processType == AppConstants.GetClientService)
                    {
                        finalResponse = await clientData.GetClientService(clientService);
                        if (finalResponse != null)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                       
                    }
                }
                return StatusCode(AppConstants.NoContent, finalResponse);
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.InternalServerError, AppConstants.ErrorMessage);
            }
        }
        private Object getObject(JsonElement clientInfo)
        {
            try
            {
                string clientString = clientInfo.GetRawText();
                JObject obj = JObject.Parse(clientString);
                var processType = obj.GetValue("processType").ToString();

                if (processType == AppConstants.Insert || processType == AppConstants.Update
                    || processType == AppConstants.Delete || processType == AppConstants.Get)
                {
                    clientObject = JsonConvert.DeserializeObject<Client>(clientString);
                }
                else if (processType == AppConstants.InsertClientService || processType == AppConstants.UpdateClientService
                    || processType == AppConstants.DeleteClientService || processType == AppConstants.GetClientService
                    || processType == AppConstants.GetClientServiceByClientId || processType == AppConstants.GetClientServiceByServiceId)
                {
                    clientObject = JsonConvert.DeserializeObject<ClientService>(clientString);
                }
                return clientObject;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }

}