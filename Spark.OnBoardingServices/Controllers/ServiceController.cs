﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.DAO.Repository;
using Spark.OnBoardingServices.Models;
using Spark.OnBoardingServices.Transactions;
using Spark.Utilities;

namespace Spark.OnBoardingServices.Controllers
{
    [Route("api/psonboardingservices")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private IService serviceData = new ServiceData();
        private Object serviceObject;

        [HttpPost("registerservice")]
        public async Task<ActionResult<dynamic>> RegisterService([FromBody] JsonElement service)
        {
            dynamic finalResponse = null;
            try
            {
                var serviceObject = getObject(service);
                if (serviceObject is Service)
                {
                    Service serviceInfo = (Service)serviceObject;
                    if (serviceInfo.processType == AppConstants.Insert)
                    {
                        finalResponse = await serviceData.InsertService(serviceInfo);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (serviceInfo.processType == AppConstants.Update)
                    {
                        finalResponse = await serviceData.UpdateService(serviceInfo);
                        return StatusCode(AppConstants.Success, finalResponse);

                    }
                    else if (serviceInfo.processType == AppConstants.Delete)
                    {
                        UnitOfWork unitofwork = new UnitOfWork();
                        finalResponse = await unitofwork.serviceDelete(serviceInfo);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (serviceInfo.processType == AppConstants.Get)
                    {
                        if (serviceInfo.serviceId.HasValue)
                        {
                            finalResponse = await serviceData.GetService(serviceInfo.serviceId);
                            if (finalResponse != null)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else if (serviceInfo.startIndex.HasValue)
                        {
                            finalResponse = await serviceData.GetService(serviceInfo.startIndex, serviceInfo.limit);
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                        else
                        {
                            finalResponse = await serviceData.GetService();
                            if (finalResponse != null && finalResponse.Count > 0)
                            {
                                return StatusCode(AppConstants.Success, finalResponse);
                            }
                        }
                    }
                }
                else if (serviceObject is Role)
                {
                    Role roleService = (Role)serviceObject;

                    if (roleService.processType == AppConstants.InsertRoleService)
                    {
                        finalResponse = await serviceData.InsertRoleService(roleService);
                        return StatusCode(AppConstants.Created, finalResponse);
                    }
                    else if (roleService.processType == AppConstants.UpdateRoleService)
                    {
                        finalResponse = await serviceData.UpdateRoleService(roleService);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (roleService.processType == AppConstants.DeleteRoleService)
                    {
                        finalResponse = await serviceData.DeleteRoleService(roleService,null,null);
                        return StatusCode(AppConstants.Success, finalResponse);
                    }
                    else if (roleService.processType == AppConstants.GetRoleServiceByRoleId)
                    {
                        finalResponse = await serviceData.GetRoleServiceByRoleId(roleService.roleId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (roleService.processType == AppConstants.GetRoleServiceByServiceId)
                    {
                        finalResponse = await serviceData.GetRoleServiceByServiceId(roleService.serviceId);
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    } 
                    else if (roleService.processType == AppConstants.GetRoleService)
                    {
                        finalResponse = await serviceData.GetRoleService(roleService);
                        if (finalResponse != null)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                    else if (roleService.processType == AppConstants.GetRoles)
                    {
                        finalResponse = await serviceData.GetRoles();
                        if (finalResponse != null && finalResponse.Count > 0)
                        {
                            return StatusCode(AppConstants.Success, finalResponse);
                        }
                    }
                }
                return StatusCode(AppConstants.NoContent, finalResponse);
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                return StatusCode(AppConstants.InternalServerError, AppConstants.ErrorMessage);
            }
        }
        private Object getObject(JsonElement serviceInfo)
        {

            String serviceString = serviceInfo.GetRawText();
            JObject obj = JObject.Parse(serviceString);
            var processType = obj.GetValue("processType").ToString();

            if (processType == AppConstants.Insert || processType == AppConstants.Update
                || processType == AppConstants.Delete || processType == AppConstants.Get)
            {
                serviceObject = JsonConvert.DeserializeObject<Service>(serviceString);              
            }
            else if (processType == AppConstants.InsertRoleService || processType == AppConstants.UpdateRoleService
                || processType == AppConstants.DeleteRoleService || processType == AppConstants.GetRoleService
                || processType == AppConstants.GetRoleServiceByRoleId || processType == AppConstants.GetRoleServiceByServiceId
                || processType == AppConstants.GetRoles)
            {
                serviceObject = JsonConvert.DeserializeObject<Role>(serviceString); 
            }
            return serviceObject; 
        }
    }
}
