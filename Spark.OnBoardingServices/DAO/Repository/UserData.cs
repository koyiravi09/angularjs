﻿using Spark.OnBoardingServices.DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Spark.Utilities;
using System.Data.SqlClient;
using Dapper;
using Spark.OnBoardingServices.Models;
using System.Data;
using Spark.OnBoardingServices.Common;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Spark.OnBoardingServices.DAO.Repository
{
    public class UserData : Connectors, IUser
    {
        #region Methods

        #region Insert User
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertUser(RegisterUser user)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), Constants.appSettingJson));
            var root = builder.Build();
            var password = root.GetSection("Password").Value;
            Int32? id = 0;
            try
            {
                var userPassword = BCrypt.Net.BCrypt.HashPassword(password); 
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_user (firstName, lastName, displayName, emailId,isLDAP, password, department, createdBy,
                                      createdOn)
                                      VALUES( @firstName, @lastName, @displayName, @emailId,@isLDAP, '" + userPassword + "' , @department, @logInUserId,'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";
                    
                    var result = await connection.QueryAsync<int>(sqlStatement, user);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region Update User
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateUser(RegisterUser user)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @" UPDATE ps.tbl_user  SET  firstName = @firstName,  lastName = @lastName,  displayName = @displayName,
                                        emailId = @emailId, department = @department,
                                        updatedBy = @logInUserId, updatedOn ='" + currentDate + "' WHERE userId = @userId ";
                    await connection.ExecuteAsync(sqlStatement, user);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region Delete User
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteUser(Int32? userId,Int32? logInUserId, SqlConnection connection, SqlTransaction transaction)
        {
            var currentDate = DateTime.Now;
            try
            {
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_user SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "'  WHERE userId = @userId";
                    await connection.ExecuteAsync(sqlStatement, new { userId = userId , logInUserId = logInUserId}, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region Get User By Id
        /// <summary>
        /// Get User By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<User> GetUser(int? userId)
        {
            User user;
            try
            {
               // Console.WriteLine("Inside the GetUser" + userId);
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @" SELECT userId,firstName,lastName,displayName,emailId, department, isActive   
                                FROM ps.tbl_user WHERE userId = " + userId + "AND isDeleted = 0";
                    user = await connection.QueryFirstOrDefaultAsync<User>(query, new { userId = userId });
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return user;
        }
        #endregion

        #region Get User By Limit
        /// <summary>
        /// Get User By Limit
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<User>> GetUser(Int32? startIndex, Int32? limit)
        {
            IEnumerable<User> users;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT userId, firstName, lastName, displayName, emailId, department, isActive                                   
                                    FROM ps.tbl_user  WHERE isDeleted = 0 ORDER BY [userId]  OFFSET " + startIndex + @" ROWS FETCH NEXT " + limit + @"ROWS ONLY";
                    users = await connection.QueryAsync<User>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return users.ToList();
        }
        #endregion

        #region Get User
        /// <summary>
        /// Get User
        /// </summary>
        /// <returns></returns>
        public async Task<List<User>> GetUser()
        {
            IEnumerable<User> users;
            try
            {
               // Console.WriteLine("@@@@@@@@@@@@@@@@@ GetUser" + connectionString);
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT userId, firstName, lastName, displayName, emailId, department, isActive                                   
                               FROM ps.tbl_user WHERE isDeleted = 0";
                    users = await connection.QueryAsync<User>(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception" + ex);
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return users.ToList();
        }
        #endregion

        #region Insert ClientUser
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientUser"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertClientUser(ClientUser clientUser)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_clientUserMapping (clientId, userId,createdBy,createdOn)
                                       VALUES (@clientId, @userId,@logInUserId,'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";
                   var result = await connection.QueryAsync<int>(sqlStatement, clientUser);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region Update Client User
        /// <summary>
        /// Update Client User
        /// </summary>
        /// <param name="clientUser"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateClientUser(ClientUser clientUser)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_clientUserMapping SET  clientId = @clientId, userId = @UserId,
                                       updatedBy = @logInUserId,updatedOn ='" + currentDate + "' WHERE clientUserMappingId = @clientUserMappingId";
                    await connection.ExecuteAsync(sqlStatement, clientUser);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region Delete Client User
        /// <summary>
        /// Delete Client User
        /// </summary>
        /// <param name="clientUser"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClientUser(ClientUser clientUser, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var con = new SqlConnection(connectionString))
                {
                    await con.OpenAsync();
                    var sqlStatement = "UPDATE ps.tbl_clientUserMapping SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "' WHERE clientId = @clientId And userId = @userId";
                    await con.ExecuteAsync(sqlStatement, new { clientId = clientUser.clientId, userId = clientUser.userId, logInUserId = clientUser.logInUserId});
                }                
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetClientUserByClientId
        /// <summary>
        /// GetClientUserByClientId
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<dynamic> GetClientUserByClientId(Int32? clientId)
        {
            dynamic clientUsers;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT cum.clientUserMappingId, cum.clientId, cum.userId, c.clientName, c.clientDescription, u.firstName, u.lastName, u.displayName From ps.tbl_clientUserMapping cum
                                 INNER JOIN ps.tbl_client c  ON c.clientId = cum.clientId AND c.isDeleted = 0
                                 INNER JOIN ps.tbl_user u ON u.userId = cum.userId AND u.isDeleted = 0
                                 WHERE cum.isDeleted = 0  AND cum.clientId = " + clientId +"";
                    clientUsers = await connection.QueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientUsers;
        }
        #endregion

        #region GetClientUserByUserId
        /// <summary>
        /// GetClientUserByUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<dynamic> GetClientUserByUserId(Int32? userId)
        {
            dynamic clientUsers;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT cum.clientUserMappingId, cum.clientId, cum.userId, c.clientName, c.clientDescription, u.firstName, u.lastName, u.displayName  From ps.tbl_clientUserMapping cum
                                 INNER JOIN ps.tbl_client c  ON c.clientId = cum.clientId AND c.isDeleted = 0
                                 INNER JOIN ps.tbl_user u ON u.userId = cum.userId AND u.isDeleted = 0 
                                 WHERE cum.isDeleted = 0 AND cum.userId = " + userId + "";
                    clientUsers = await connection.QueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientUsers;
        }
        #endregion

        #region GetClientUser
        /// <summary>
        /// GetClientUser
        /// </summary>
        /// <param name="clientUser"></param>
        /// <returns></returns>
        public async Task<dynamic> GetClientUser(ClientUser clientUser)
        {
            dynamic clientUsers;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT cum.clientUserMappingId, cum.clientId, cum.userId, c.clientName, u.firstName, c.clientDescription From ps.tbl_clientUserMapping cum
                                 INNER JOIN ps.tbl_client c  ON c.clientId = cum.clientId AND c.isDeleted = 0
                                 INNER JOIN ps.tbl_user u ON u.userId = cum.userId AND u.isDeleted = 0 
                                 WHERE cum.isDeleted = 0 AND cum.clientId = @clientId AND cum.userId = @userId";
                    clientUsers = await connection.QueryFirstOrDefaultAsync(query,clientUser);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientUsers;
        }
        #endregion

        #region InsertUserRole
        /// <summary>
        /// InsertUserRole
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertUserRole(UserRole userRole)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_userRoleMapping ( roleId, userId,createdBy,createdOn)
                                       VALUES (@roleId, @userId, @logInUserId,'" + currentDate + "')SELECT CAST(SCOPE_IDENTITY() as int)";

                    var result = await connection.QueryAsync<int>(sqlStatement, userRole);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id,1);
        }
        #endregion

        #region UpdateUserRole
        /// <summary>
        /// UpdateUserRole
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateUserRole(UserRole userRole)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_userRoleMapping SET roleId = @roleId, userId = @userId, updatedBy = @logInUserId, updatedOn = '" + currentDate + "'WHERE userRoleMappingId = @userRoleMappingId";
                    await connection.ExecuteAsync(sqlStatement, userRole);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteUserRole
        /// <summary>
        /// DeleteUserRole
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteUserRole(UserRole userRole, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var Connection = new SqlConnection(connectionString))
                {
                      await Connection.OpenAsync();
                      var sqlStatement = "UPDATE ps.tbl_userRoleMapping SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "' WHERE roleId = @roleId And userId = @userId";
                      await Connection.ExecuteAsync(sqlStatement, userRole);
                }
                
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetUserRoleByUserId
        /// <summary>
        /// GetUserRoleByUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<dynamic> GetUserRoleByUserId(Int32? userId)
        {
            dynamic userRoles;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT urm.userRoleMappingId, urm.roleId, urm.userId, r.roleName, r.roleDescription, u.firstName, u.lastName, u.displayName From ps.tbl_userRoleMapping urm
                                 INNER JOIN ps.tbl_user u ON u.userId = urm.userId AND u.isDeleted = 0 
                                 INNER JOIN ps.tbl_role r  ON r.roleId = urm.roleId AND r.isDeleted = 0
                                 WHERE urm.isDeleted = 0 AND urm.userId =" + userId + "";
                    userRoles = await connection.QueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return userRoles;
        }
        #endregion

        #region GetUserRoleByRoleId
        /// <summary>
        /// GetUserRoleByRoleId
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<dynamic> GetUserRoleByRoleId(Int32? roleId)
        {
            dynamic userRoles;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT urm.userRoleMappingId, urm.roleId, urm.userId, r.roleName, r.roleDescription, u.firstName, u.lastName, u.displayName From ps.tbl_userRoleMapping urm
                                 INNER JOIN ps.tbl_user u ON u.userId = urm.userId AND u.isDeleted = 0                                
                                 INNER JOIN ps.tbl_role r  ON r.roleId = urm.roleId AND r.isDeleted = 0
                                 WHERE urm.isDeleted = 0 AND urm.roleId =" + roleId + "";
                    userRoles = await connection.QueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return userRoles;
        }
        #endregion

        #region GetUserRoleByClientServiceRoleId
        public async Task<dynamic> GetUserRoleByClientServiceRoleId(UserRole userRole)
        {
            dynamic userRoles;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT distinct urm.userRoleMappingId, urm.userId, urm.roleId, u.firstName, u.lastName, u.displayName, u.emailId, r.roleName, r.roleDescription From ps.tbl_userRoleMapping urm
                                INNER JOIN ps.tbl_user u ON u.userId = urm.userId AND u.isDeleted = 0                                
                                INNER JOIN ps.tbl_role r  ON r.roleId = urm.roleId AND r.isDeleted = 0
                                iNNER jOIN ps.tbl_clientServiceMapping csm ON csm.serviceId = r.serviceId AND csm.isDeleted =0  
                                WHERE urm.isDeleted = 0 AND urm.roleId = @roleId AND csm.serviceId = @serviceId AND csm.clientId = @clientId";
                    userRoles = await connection.QueryAsync(query,userRole);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return userRoles;
        }
        #endregion

        #region GetUserRole
        /// <summary>
        /// GetUserRole
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public async Task<dynamic> GetUserRole(UserRole userRole)
        {
            dynamic userRoles;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT urm.userRoleMappingId,urm.roleId, urm.userId, r.roleName, u.firstName, r.roleDescription From ps.tbl_userRoleMapping urm
                                 INNER JOIN ps.tbl_user u ON u.userId = urm.userId AND u.isDeleted = 0 
                                 INNER JOIN ps.tbl_role r ON r.roleId = urm.roleId AND r.isDeleted = 0
                                 WHERE urm.isDeleted = 0 AND urm.roleId = @roleId AND urm.userId = @userId";
                    userRoles = await connection.QueryFirstOrDefaultAsync(query,userRole);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return userRoles;
        }

        #endregion


        #region DeleteClientUserByUserId
        /// <summary>
        /// DeleteClientUserByUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClientUserByUserId(Int32? userId,Int32? logInUserId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_clientUserMapping SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "' WHERE userId = @userId";                   
                    await connection.ExecuteAsync(sqlStatement, new { userId = userId, logInUserId = logInUserId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }

        #endregion

        #region DeleteUserRoleByUserId
        /// <summary>
        /// DeleteUserRoleByUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteUserRoleByUserId(Int32? userId, Int32? logInUserId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_userRoleMapping SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "' WHERE userId = @userId";
                    await connection.ExecuteAsync(sqlStatement, new { userId = userId , logInUserId = logInUserId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }

        public async Task<ResponseModel> DeleteClientUserByClientId(Int32? clientId, Int32? logInUserId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_clientUserMapping SET isDeleted = 1, updatedBy = @logInUserId, updatedOn = '" + currentDate + "'  WHERE clientId = @clientId";
                    await connection.ExecuteAsync(sqlStatement, new { clientId = clientId , logInUserId = logInUserId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);

        }
        #endregion

        #region GetUserByEmailId
        /// <summary>
        /// GetUserByEmailId
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public  async Task<dynamic> GetUserByEmailId(string emailId)
        {
            dynamic user = null;
 
            try
            {
              
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT u.userId,u.firstName,u.lastName,r.roleName ,r.roleId FROM  ps.tbl_user u 
                                   INNER JOIN ps.tbl_userRoleMapping ur ON ur.userId = u.userId  AND ur.isDeleted = 0
                                   INNER JOIN ps.tbl_role r ON r.roleId = ur.roleId AND r.isDeleted = 0
                                   WHERE emailId = '" + emailId + "' AND u.isDeleted = 0 ";
                     var result = await connection.QueryAsync(query );

                    if (result != null && result?.ToList().Count > 0)
                    {
                        Dictionary<int, Object> dicTask = new Dictionary<int, Object>();
                        foreach (var item in result)
                        {
                            if (!dicTask.TryGetValue(item.userId, out user))
                            {
                                user = new ExpandoObject();
                                user.firstName = item.firstName;
                                user.lastName = item.lastName;
                                user.userId = item.userId;
                                user.roles = new List<dynamic>();
                                dynamic role = new ExpandoObject();
                                role.roleId = item.roleId;
                                role.roleName = item.roleName;
                                user.roles.Add(role);
                                dicTask.Add(item.userId, user);
                            }
                            else
                            {
                                dynamic role = new ExpandoObject();
                                role.roleId = item.roleId;
                                role.roleName = item.roleName;
                                user.roles.Add(role);
                            }

                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return user;
        }
        #endregion

        #region
        public async Task<dynamic> GetUserRoleByRoleIds(string roleIds)
        {
            IEnumerable<dynamic> userRoles;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT urm.userRoleMappingId,urm.roleId, urm.userId, r.roleName, u.displayName, r.roleDescription From ps.tbl_userRoleMapping urm
                                 INNER JOIN ps.tbl_user u ON u.userId = urm.userId AND u.isDeleted = 0 
                                 INNER JOIN ps.tbl_role r ON r.roleId = urm.roleId AND r.isDeleted = 0 AND urm.isDeleted=0
								 WHERE urm.roleId IN (" + roleIds+")";
                    userRoles = await connection.QueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return userRoles.ToList();
        }
        #endregion
#endregion
    }
}

