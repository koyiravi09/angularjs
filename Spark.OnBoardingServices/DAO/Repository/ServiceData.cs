﻿using Dapper;
using Spark.Utilities;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Spark.OnBoardingServices.Common;

namespace Spark.OnBoardingServices.DAO.Repository
{
    public class ServiceData : Connectors, IService
    {
        #region Methods
        #region InsertService
        /// <summary>
        /// InsertService
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertService(Service service)
        {
            Int32? id = 0;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    var currentDate = DateTime.Now;
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_service (serviceName, serviceDescription,
                                       isActive, createdBy, createdOn)
                                       VALUES (@serviceName, @serviceDescription, @isActive,
                                       @userId, '" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";
                   var result = await connection.QueryAsync<int>(sqlStatement, service);
                   id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region UpdateService
        /// <summary>
        /// UpdateService
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateService(Service service)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    var currentDate = DateTime.Now;
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_service SET serviceName = @serviceName, serviceDescription = @serviceDescription, 
                                       isActive = @isActive, updatedBy=@userId, updatedOn = '" + currentDate + "' WHERE serviceId = @serviceId";
                    await connection.ExecuteAsync(sqlStatement, service);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteService
        /// <summary>
        /// DeleteService
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteService(Int32? serviceId,Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_service SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE serviceId = @serviceId";
                    await connection.ExecuteAsync(sqlStatement, new { serviceId = serviceId, userId = userId },transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetService By Id
        /// <summary>
        /// GetService By Id
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public async Task<Service> GetService(Int32? serviceId)
        {
            Service service;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT serviceId, serviceName, serviceDescription, isActive  
                                FROM ps.tbl_service WHERE serviceId = @serviceId AND isDeleted = 0";
                    service = await connection.QuerySingleAsync<Service>(query, new { serviceId = serviceId });
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return service;
        }
        #endregion

        #region GetService By Limit
        /// <summary>
        /// GetService By Limit
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Service>> GetService(Int32? startIndex, Int32? limit)
        {
            IEnumerable<Service> services;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT serviceId, serviceName, serviceDescription, isActive
                                FROM ps.tbl_service WHERE isDeleted = 0 ORDER BY [serviceId] OFFSET " + startIndex + @" ROWS FETCH NEXT " + limit + @"ROWS ONLY";
                    services = await connection.QueryAsync<Service>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return services.ToList();
        }
        #endregion

        #region GetService
        /// <summary>
        /// GetService
        /// </summary>
        /// <returns></returns>
        public async Task<List<Service>> GetService()
        {
            IEnumerable<Service> services;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT serviceId, serviceName, serviceDescription, isActive 
                                 FROM ps.tbl_service WHERE isDeleted = 0";
                    services = await connection.QueryAsync<Service>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return services.ToList();
        }
        #endregion

        #region InsertRoleService
        /// <summary>
        /// InsertRoleService
        /// </summary>
        /// <param name="roleService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertRoleService(Role roleService)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now ;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_role (roleName, roleDescription, serviceId, IsActive, createdBy, createdOn)
                                       VALUES (@roleName, @roleDescription, @serviceId, @IsActive, @userId, '" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                   var result = await connection.QueryAsync<int>(sqlStatement, roleService);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region UpdateRoleService
        /// <summary>
        /// UpdateRoleService
        /// </summary>
        /// <param name="roleService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateRoleService(Role roleService)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_role SET roleName=@roleName, roleDescription = @roleDescription,
                                       serviceId = @serviceId, isActive = @isActive, updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE roleId = @roleId";
                    await connection.ExecuteAsync(sqlStatement, roleService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteRoleService
        /// <summary>
        /// DeleteRoleService
        /// </summary>
        /// <param name="roleService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteRoleService(Role roleService, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now; 
                using (var con = new SqlConnection(connectionString))
                {
                    await con.OpenAsync();
                    var sqlStatement = "UPDATE ps.tbl_role SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE roleId = @roleId And serviceId = @serviceId";
                    await con.ExecuteAsync(sqlStatement, new { roleId = roleService.roleId, serviceId = roleService.serviceId, roleService.userId });
                }            
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region DeleteRoleServiceByServiceId
        /// <summary>
        /// DeleteRoleServiceByServiceId
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteRoleServiceByServiceId(Int32? serviceId,Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            var currentDate = DateTime.Now;
            try
            {
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_role SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE serviceId = @serviceId";
                    await connection.ExecuteAsync(sqlStatement, new { serviceId = serviceId, userId=userId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region DeleteClientServiceByServiceId
        /// <summary>
        /// DeleteClientServiceByServiceId
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClientServiceByServiceId(Int32? serviceId,Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_clientServiceMapping SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE serviceId = @serviceId";
                    await connection.ExecuteAsync(sqlStatement, new { serviceId = serviceId, userId = userId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetRoleServiceByRoleId
        /// <summary>
        /// GetRoleServiceByRoleId
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<List<Role>> GetRoleServiceByRoleId(int? roleId)
        {
            IEnumerable<Role> roleServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT r.roleId, r.roleName, s.serviceDescription,r.roleDescription, r.serviceId, r.isActive From ps.tbl_role r
                                INNER JOIN ps.tbl_service s ON s.serviceId = r.serviceId AND s.isDeleted = 0 
                                WHERE r.isDeleted = 0 AND r.roleId = " + roleId +"";
                    roleServices = await connection.QueryAsync<Role>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return roleServices.ToList();
        }
        #endregion

        #region GetRoleServiceByServiceId
        /// <summary>
        /// GetRoleServiceByServiceId
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public async Task<List<Role>> GetRoleServiceByServiceId(Int32? serviceId)
        {
            IEnumerable<Role> roleServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT r.roleId, r.roleName, s.serviceDescription, r.serviceId,r.roleDescription, r.isActive From ps.tbl_role r
                                INNER JOIN ps.tbl_service s ON s.serviceId = r.serviceId AND s.isDeleted = 0 
                                WHERE r.isDeleted = 0 AND r.serviceId = " + serviceId+"";
                    roleServices = await connection.QueryAsync<Role>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return roleServices.ToList();

        }
        #endregion

        #region GetRoleService
        /// <summary>
        /// GetRoleService
        /// </summary>
        /// <param name="roleService"></param>
        /// <returns></returns>
        public async Task<dynamic> GetRoleService(Role roleService)
        {
            dynamic roleServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT r.roleId, r.roleName, s.serviceDescription,r.roleDescription, r.serviceId, r.isActive From ps.tbl_role r
                                INNER JOIN ps.tbl_service s ON s.serviceId = r.serviceId AND s.isDeleted = 0 
                                WHERE r.isDeleted = 0 AND r.roleId = @roleId AND r.serviceId = @serviceId";
                    roleServices = await connection.QueryFirstOrDefaultAsync(query,roleService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return roleServices;
        }

        #endregion

        #region GetRoles
        public async Task<List<dynamic>> GetRoles()
        {
            IEnumerable<dynamic> role;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT r.roleId,r.roleName, r.roleDescription,s.serviceId,s.serviceName FROM ps.tbl_role r
                                  INNER JOIN Ps.tbl_service s  ON s.serviceId = r.serviceId AND s.isDeleted = 0
                                  WHERE r.isDeleted = 0";
                    role = await connection.QueryAsync<dynamic>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return role.ToList();
        }
        #endregion

        #endregion
    }
}
