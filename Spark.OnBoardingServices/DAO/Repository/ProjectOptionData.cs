﻿using Amazon.Runtime.Internal.Util;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spark.OnBoardingServices.Common;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.Models;
using Spark.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Logger = Spark.Utilities.Logger;

namespace Spark.OnBoardingServices.DAO.Repository
{
    public class ProjectOptionData : Connectors, IProjectOption

    {

        public async Task<ResponseModel> InsertComplianceRequirements(ProjectOption projectOption)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                var complianceRequirement = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.complianceRequirements);

                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_projectOptions (projectId, complianceRequirement, createdBy,createdOn)
                                      VALUES( "+projectOption.projectId+" ,'"+ complianceRequirement + "' ,"+projectOption.userId+",'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                    var result = await connection.QueryAsync<int>(sqlStatement);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);

        }

        public async Task<ResponseModel> InsertStorageArchivalRequirements(ProjectOption projectOption)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                var storageArchivalRequirement = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.storageArchivalRequirement);

                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_projectOptions (projectId, storageArchivalRequirement, createdBy,createdOn)
                                      VALUES( " + projectOption.projectId + " ,'" + storageArchivalRequirement + "' ," + projectOption.userId + ",'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                    var result = await connection.QueryAsync<int>(sqlStatement);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);

        }

        public async Task<ResponseModel> InsertTranslationRequirements(ProjectOption projectOption)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                var translationRequirement = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.translationRequirements);

                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_projectOptions (projectId, translationRequirement, createdBy,createdOn)
                                      VALUES( " + projectOption.projectId + " ,'" + translationRequirement + "' ," + projectOption.userId + ",'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                    var result = await connection.QueryAsync<int>(sqlStatement);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);

        }

        public async Task<ResponseModel> UpdateComplianceRequirements(ProjectOption projectOption)
        {
            try
            {
                var currentDate = DateTime.Now;
                var complianceRequirements = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.complianceRequirements);
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_projectOptions SET projectId = " + projectOption.projectId + ", complianceRequirement = '"+ complianceRequirements +
                                      "', updatedBy = " + projectOption.userId + ", updatedOn = '" + currentDate + "' WHERE projectOptionId = "+projectOption.projectOptionId+"";
                    await connection.ExecuteAsync(sqlStatement);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }

            return CommonUtil.ResponseObject(null, 2);
        }

        public async Task<ResponseModel> UpdateStorageArchivalRequirements(ProjectOption projectOption)
        {
            try
            {
                          
                var currentDate = DateTime.Now;
                var storageArchivalRequirements = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.storageArchivalRequirement);
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_projectOptions SET projectId = " + projectOption.projectId + ", storageArchivalRequirement = '" + storageArchivalRequirements +
                                      "' , updatedBy = "+ projectOption.userId +", updatedOn = '" + currentDate + "' WHERE projectOptionId = " + projectOption.projectOptionId + "";
                    await connection.ExecuteAsync(sqlStatement);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }

            return CommonUtil.ResponseObject(null, 2);
        }

        public async Task<ResponseModel> UpdateTranslationRequirements(ProjectOption projectOption)
        {
            try
            {
                var currentDate = DateTime.Now;
                var translationRequirements = Newtonsoft.Json.JsonConvert.SerializeObject(projectOption.translationRequirements);
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_projectOptions SET projectId = " + projectOption.projectId + ", translationRequirement ='" + translationRequirements +
                                      "', updatedBy = " + projectOption.userId + ", updatedOn = '" + currentDate + "' WHERE projectOptionId = " + projectOption.projectOptionId + "";
                    await connection.ExecuteAsync(sqlStatement);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }

            return CommonUtil.ResponseObject(null, 2);

        }
        public async Task<dynamic> GetProjectOption(ProjectOption projectOption)
        {
            dynamic projectOptionResponse = null;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT projectOptionId,p.clientProjectId as projectId,translationRequirement,
                                        complianceRequirement,
                                        storageArchivalRequirement 
                                    from ps.tbl_project p 
                                    left join ps.tbl_projectOptions po 
	                                    on po.projectId = p.clientProjectId and po.isDeleted = 0
                                    WHERE p.isDeleted = 0 and p.clientProjectId = "+ projectOption.projectId + "";

                   //var result =  connection.Query<dynamic>(query).FirstOrDefault();
                    var result = await connection.QueryFirstOrDefaultAsync(query);
                    if (result != null)
                    {
                        projectOptionResponse = new ExpandoObject();
                        if (result.translationRequirement != null)
                        {
                            projectOptionResponse.translationRequirement = JsonConvert.DeserializeObject<List<TranslationRequirements>>(result.translationRequirement);
                        
                        }
                        else
                        {
                            projectOptionResponse.translationRequirement = null;
                        }
                        if (result.complianceRequirement != null)
                        {
                            projectOptionResponse.complianceRequirement = JsonConvert.DeserializeObject<List<ComplianceRequirements>>(result.complianceRequirement);
                           
                        }
                        else
                        {
                            projectOptionResponse.complianceRequirement = null;
                        }
                        if (result.storageArchivalRequirement != null)
                        {
                           
                            projectOptionResponse.storageArchivalRequirement = JsonConvert.DeserializeObject<StorageArcheivalRequirements>(result.storageArchivalRequirement);
                           
                        }
                        else
                        {
                            projectOptionResponse.storageArchivalRequirement = null;
                        }

                    }
                    projectOptionResponse.projectOptionId = result?.projectOptionId;
                    projectOptionResponse.projectId = result?.projectId;

                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return projectOptionResponse;
        }
        
         
       
    }
}
