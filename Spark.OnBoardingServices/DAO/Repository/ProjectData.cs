﻿using Dapper;
using Spark.Utilities;
using Spark.OnBoardingServices.DAO.Interface;
using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Spark.OnBoardingServices.Common;

namespace Spark.OnBoardingServices.DAO.Repository
{
    public class ProjectData : Connectors, IProject
    {
        #region Methods

        #region InsertProject
        /// <summary>
        /// InsertProject
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertProject(Project project)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_project (clientId, projectName, projectDescription, projectStatusId, createdBy, createdOn)
                                       VALUES (@clientId, @projectName,@projectDescription,@projectStatusId,@userId,'" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";
                    var result = await connection.QueryAsync<int>(sqlStatement, project);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region UpdateProject
        /// <summary>
        /// UpdateProject
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateProject(Project project)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_project SET  projectName = @projectName,projectStatusId = @projectStatusId,
                                       projectDescription = @projectDescription,  updatedBy = @userId, 
                                       updatedOn = '" + currentDate + "' WHERE clientProjectId = @clientProjectId";
                    await connection.ExecuteAsync(sqlStatement, project);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region UpdateProjectStatus
        public async Task<ResponseModel> UpdateProjectStatus(Project project)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_project SET projectStatusId = @projectStatusId, updatedBy = @userId, updatedOn = '" + currentDate + "'" +
                                       " WHERE clientProjectId = @clientProjectId " ;
                    await connection.ExecuteAsync(sqlStatement, project);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteProject
        /// <summary>
        /// DeleteProject
        /// </summary>
        /// <param name="clientProjectId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteProject(Int32? clientProjectId, Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_project SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE clientProjectId = @clientProjectId";
                    await connection.ExecuteAsync(sqlStatement, new { clientProjectId = clientProjectId, userId = userId }, transaction);
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }

        #endregion

        #region GetProject By Id
        /// <summary>
        /// GetProject By Id
        /// </summary>
        /// <param name="clientProjectId"></param>
        /// <returns></returns>
        public async Task<dynamic> GetProject(Int32? clientProjectId)
        {
            dynamic projects;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @" SELECT p.clientProjectId, p.clientId, c.bucketName,c.clientName, p.projectName, p.projectDescription, p.projectStatusId ,
                                  ISNULL(p.updatedOn,p.createdOn) as lastProjectUpdatedDateTime , ISNULL(c.updatedOn,c.createdOn) as lastClientUpdatedDateTime 
                                FROM ps.tbl_project p INNER JOIN  ps.tbl_client c on p.clientId = c.clientId WHERE p.clientProjectId = @clientProjectId AND p.isDeleted = 0";
                    projects = await connection.QueryFirstOrDefaultAsync(query, new { clientProjectId = clientProjectId });
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return projects;
        }
        #endregion

        #region GetProject By Limit
        /// <summary>
        /// GetProject By Limit
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Project>> GetProject(Int32? startIndex, Int32? limit)
        {
            IEnumerable<Project> projects;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT p.clientProjectId, p.clientId, c.bucketName, c.clientName, p.projectName, p.projectDescription, p.projectStatusId,
                                 ISNULL(p.updatedOn,p.createdOn) as lastProjectUpdatedDateTime , ISNULL(c.updatedOn,c.createdOn) as lastClientUpdatedDateTime                                  
                                 FROM ps.tbl_project p INNER JOIN ps.tbl_client c on p.clientId = c.clientId WHERE p.isDeleted = 0 ORDER BY [clientProjectId] OFFSET " + startIndex + @" ROWS FETCH NEXT " + limit + @" ROWS ONLY";

                    projects = await connection.QueryAsync<Project>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return projects.ToList();
        }
        #endregion

        #region GetProject
        /// <summary>
        /// GetProject
        /// </summary>
        /// <returns></returns>
        public async Task<dynamic> GetProject()
        {
            dynamic projects;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT p.clientProjectId, p.clientId, c.bucketName,c.clientName, p.projectName, p.projectDescription, p.projectStatusId,
                                p.createdBy, p.createdOn, p.updatedBy, p.updatedOn, p.isDeleted ,
                               ISNULL(p.updatedOn,p.createdOn) as lastProjectUpdatedDateTime , ISNULL(c.updatedOn,c.createdOn) as lastClientUpdatedDateTime 
                                FROM ps.tbl_project p INNER JOIN ps.tbl_client c on p.clientId = c.clientId WHERE p.isDeleted = 0";

                    projects = await connection.QueryAsync(query);

                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return projects;
        }
        #endregion

        #region InsertProjectService
        public async Task<ResponseModel> InsertProjectService(ProjectService projectService)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_projectService (projectId, serviceId, createdBy, createdOn)
                                       VALUES (@clientProjectId, @serviceId, @userId, '" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                    var result = await connection.QueryAsync<int>(sqlStatement, projectService);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region UpdateProjectService
        public async Task<ResponseModel> UpdateProjectService(ProjectService projectService)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_projectService SET projectId = @clientProjectId, serviceId = @serviceId, 
                                        updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE projectServiceId = @projectServiceId";
                    await connection.ExecuteAsync(sqlStatement, projectService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteProjectService
        public async Task<ResponseModel> DeleteProjectService(ProjectService projectService, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var con = new SqlConnection(connectionString))
                {
                    await con.OpenAsync();
                    var sqlStatement = "UPDATE ps.tbl_projectService SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE projectId = @clientProjectId And serviceId = @serviceId";
                    await con.ExecuteAsync(sqlStatement, new { clientProjectId = projectService.clientProjectId, serviceId = projectService.serviceId, userId = projectService.userId });
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetProjectServiceByProjectId
        public async Task<List<ProjectService>> GetProjectServiceByProjectId(Int32? clientProjectId)
        {       
                IEnumerable<ProjectService> projectServices;
                try
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        await connection.OpenAsync();
                        var query = @"SELECT psm.projectServiceId, p.clientProjectId, psm.serviceId, s.serviceName FROM ps.tbl_projectService psm
                                    INNER JOIN ps.tbl_project p  ON p.clientProjectId = psm.projectId AND p.isDeleted = 0
                                    INNER JOIN ps.tbl_service s ON s.serviceId = psm.serviceId AND s.isDeleted = 0                                  
                                    WHERE psm.isDeleted=0 AND psm.projectId = " + clientProjectId + "";
                        projectServices = await connection.QueryAsync<ProjectService>(query);
                    }
                }
                catch (Exception ex)
                {
                    Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                    throw ex;
                }
                return projectServices.ToList();
            
        }
        #endregion

        #region GetProjectService
        public async Task<dynamic> GetProjectService(ProjectService projectService)
        {
            dynamic projectServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT psm.projectServiceId, p.clientProjectId, psm.serviceId, s.serviceName FROM ps.tbl_projectService psm
                                    INNER JOIN ps.tbl_project p  ON p.clientProjectId = psm.projectId AND p.isDeleted = 0
                                    INNER JOIN ps.tbl_service s ON s.serviceId = psm.serviceId AND s.isDeleted = 0                                  
                                    WHERE psm.isDeleted=0 AND psm.projectId = @clientProjectId AND psm.serviceId = @serviceId";
                    projectServices = await connection.QueryFirstOrDefaultAsync(query,projectService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return projectServices;

        }
        #endregion

        #region DeleteProjectServiceByProjectId
        public async Task<ResponseModel> DeleteProjectServiceByProjectId(Int32? clientprojectId, Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_projectService SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE projectId = @clientprojectId";
                    await connection.ExecuteAsync(sqlStatement, new { clientprojectId = clientprojectId, userId = userId }, transaction);
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region DeleteProjectServiceByServiceId
        public async Task<ResponseModel> DeleteProjectServiceByServiceId(Int32? serviceId, Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            var currentDate = DateTime.Now;
            try
            {
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_projectService SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE serviceId = @serviceId";
                    await connection.ExecuteAsync(sqlStatement, new { serviceId = serviceId, userId = userId }, transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #endregion
    }
}
