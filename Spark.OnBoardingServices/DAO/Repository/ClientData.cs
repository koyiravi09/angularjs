﻿using Spark.OnBoardingServices.DAO.Interface;
using Spark.Utilities;
using System;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Spark.OnBoardingServices.Models;
using Spark.OnBoardingServices.Common;
using System.Dynamic;

namespace Spark.OnBoardingServices.DAO.Repository
{
    public class ClientData : Connectors, IClient
    {
        #region Methods
        #region InsertClient
        /// <summary>
        ///  Insert a client information.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        /// 
        public async Task<ResponseModel> InsertClient(Client client)
        {
            Int32? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_client (clientName, clientDescription, addressLine_1,
                                       addressLine_2, postalCode, stateProvinceCountry, country, contactNumber, isActive,
                                       createdBy, createdOn,bucketName)
                                       VALUES (@clientName, @clientDescription, @addressLine_1, @addressLine_2,
                                       @postalCode, @stateProvinceCountry, @country ,@contactNumber, @isActive, @userId, '" + currentDate + "',@bucketName) SELECT CAST(SCOPE_IDENTITY() as int)";
                   var result = await connection.QueryAsync<int>(sqlStatement, client);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }

            return CommonUtil.ResponseObject(id, 1);
        }
        #endregion

        #region UpdateClient
        /// <summary>
        /// Update a client information.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateClient(Client client)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_client SET clientName = @clientName, clientDescription = @clientDescription, 
                                       addressLine_1 = @addressLine_1, addressLine_2 = @addressLine_2, postalCode = @postalCode,
                                       stateProvinceCountry = @stateProvinceCountry, country = @country, contactNumber = @contactNumber, isActive = @isActive,  
                                       updatedBy=@userId, updatedOn = '" + currentDate + "' WHERE clientId = @clientId";
                    await connection.ExecuteAsync(sqlStatement, client); 
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }

            return CommonUtil.ResponseObject(null, 2);
        }
        #endregion

        #region DeleteClient 
        /// <summary>
        /// Delete a client information.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClient(Int32? clientId, Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_client SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE clientId = @clientId";
                    await connection.ExecuteAsync(sqlStatement, new { clientId = clientId, userId = userId},transaction);
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetClient by id
        /// <summary>
        /// GetClient a clientInformation by id.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<Client> GetClient(Int32? clientId)
        {
            Client client;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT clientId, clientName, clientDescription, addressLine_1, addressLine_2, postalCode,
                                stateProvinceCountry, country, contactNumber, isActive, bucketName FROM ps.tbl_client WHERE clientId = " + clientId + " AND isDeleted = 0";
                    client = await connection.QueryFirstOrDefaultAsync<Client>(query, new { clientId = clientId });
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return client;
        }
        #endregion

        #region GetClient by start and limit
        /// <summary>
        /// GetClient information by limit
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Client>> GetClient(Int32? startIndex, Int32? limit)
        {
            IEnumerable<Client> clients;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT clientId, clientName, clientDescription, addressLine_1, addressLine_2, postalCode, 
                                stateProvinceCountry, country, contactNumber, isActive, bucketName
                                FROM ps.tbl_client WHERE isDeleted = 0 ORDER BY [clientId] OFFSET " + startIndex + @" ROWS FETCH NEXT " + limit + @" ROWS ONLY";
                    clients = await connection.QueryAsync<Client>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clients.ToList();
        }
        #endregion

        #region GetClient

        /// <summary>
        /// GetClient
        /// </summary>
        /// <returns></returns>
        public async Task<List<Client>> GetClient()
        {
            IEnumerable<Client> clients;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT clientId, clientName, clientDescription, addressLine_1, addressLine_2, postalCode,
                                stateProvinceCountry, country, contactNumber, isActive, bucketName FROM ps.tbl_client WHERE isDeleted = 0";
                    clients = await connection.QueryAsync<Client>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clients.ToList();
        }
        #endregion

        #region InsertClientService
        /// <summary>
        /// InsertClientService
        /// </summary>
        /// <param name="clientService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> InsertClientService(ClientService clientService)
        {
            int? id = 0;
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"INSERT INTO ps.tbl_clientServiceMapping (clientId, serviceId, createdBy, createdOn)
                                       VALUES (@clientId, @serviceId, @userId, '" + currentDate + "') SELECT CAST(SCOPE_IDENTITY() as int)";

                   var result = await connection.QueryAsync<int>(sqlStatement, clientService);
                    id = result?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(id,1);
        }
        #endregion

        #region UpdateClientService
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateClientService(ClientService clientService)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var sqlStatement = @"UPDATE ps.tbl_clientServiceMapping SET clientId = @clientId, serviceId = @serviceId, 
                                        createdBy = @userId, createdOn = '" + currentDate + "' WHERE clientServiceMappingId = @clientServiceMappingId";
                    await connection.ExecuteAsync(sqlStatement, clientService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null,2);
        }
        #endregion
        #region DeleteClientService
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClientService(ClientService clientService, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                using (var con = new SqlConnection(connectionString))
                {
                     await con.OpenAsync();
                     var sqlStatement = "UPDATE ps.tbl_clientServiceMapping SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "' WHERE clientId = @clientId And serviceId = @serviceId";
                     await con.ExecuteAsync(sqlStatement, new { clientId = clientService.clientId, serviceId = clientService.serviceId, userid=clientService.userId});
                }               
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region DeleteClientServiceByClientId
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientService"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteClientServiceByClientId(Int32? clientId, Int32? userId, SqlConnection connection, SqlTransaction transaction)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (connection != null)
                {
                    var sqlStatement = "UPDATE ps.tbl_clientServiceMapping SET isDeleted = 1, updatedBy = @userId, updatedOn = '" + currentDate + "'  WHERE clientId = @clientId";
                    await connection.ExecuteAsync(sqlStatement, new { clientId = clientId, userId = userId }, transaction);
                }
            }

            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return CommonUtil.ResponseObject(null, 3);
        }
        #endregion

        #region GetClientServiceByClientId
        /// <summary>
        /// GetClientServiceByClientId
        /// </summary>
        /// <param name="clienId"></param>
        /// <returns></returns>
        public async Task<List<ClientService>> GetClientServiceByClientId(Int32? clienId)
        {
            IEnumerable<ClientService> clientServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT csm.clientServiceMappingId, csm.clientId, csm.serviceId, c.clientName, s.serviceName, c.clientDescription, s.serviceDescription FROM ps.tbl_clientServiceMapping csm
                                 INNER JOIN ps.tbl_client c  ON c.clientId = csm.clientId AND c.isDeleted = 0
                                 INNER JOIN ps.tbl_service s ON s.serviceId = csm.serviceId AND s.isDeleted = 0                                  
                                 WHERE csm.isDeleted=0 AND csm.clientId = " + clienId + "";
                    clientServices = await connection.QueryAsync<ClientService>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientServices.ToList();
        }
        #endregion

        #region GetClientServiceByServiceId
        public async Task<List<ClientService>> GetClientServiceByServiceId(Int32? serviceId)
        {
            IEnumerable<ClientService> clientServices;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT csm.clientServiceMappingId, csm.clientId, csm.serviceId, c.clientName, s.serviceName, c.clientDescription, s.serviceDescription FROM ps.tbl_clientServiceMapping csm
                                 INNER JOIN ps.tbl_client c  ON c.clientId = csm.clientId AND c.isDeleted = 0                                
                                 INNER JOIN ps.tbl_service s ON s.serviceId = csm.serviceId AND s.isDeleted = 0 
                                 WHERE csm.isDeleted=0 AND csm.serviceId = " + serviceId + "";
                    clientServices = await connection.QueryAsync<ClientService>(query);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientServices.ToList();
        }
        #endregion

        #region GetClientService
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientService"></param>
        /// <returns></returns>
        public async Task<dynamic> GetClientService(ClientService clientService)
        {
            dynamic clientServices ;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var query = @"SELECT csm.clientId, csm.serviceId, c.clientName, s.serviceName, c.clientDescription, s.serviceDescription FROM ps.tbl_clientServiceMapping csm
                                 INNER JOIN ps.tbl_client c  ON c.clientId = csm.clientId AND c.isDeleted = 0
                                 INNER JOIN ps.tbl_service s ON s.serviceId = csm.serviceId AND s.isDeleted = 0 
                                 WHERE csm.isDeleted=0 AND  csm.clientId = @clientId AND csm.serviceId = @serviceId";
                    clientServices = await connection.QueryFirstOrDefaultAsync(query,clientService);
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex, (Int32)Category.Fatal, (Int32)Priority.High);
                throw ex;
            }
            return clientServices;
        }
        #endregion

        #endregion
    }

}


