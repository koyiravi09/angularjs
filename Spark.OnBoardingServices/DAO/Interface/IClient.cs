﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.DAO.Interface
{
    public interface IClient
    {
        Task<ResponseModel> InsertClient(Client client);
        Task<ResponseModel> UpdateClient(Client client);
        Task<ResponseModel> DeleteClient(Int32? clientId, Int32 ?userId, SqlConnection connection, SqlTransaction transaction);
        Task<List<Client>> GetClient(Int32? startindex, Int32? limit);
        Task<Client> GetClient(Int32? clientId);
        Task<List<Client>> GetClient();
        Task<ResponseModel> InsertClientService(ClientService clientService);
        Task<ResponseModel> UpdateClientService(ClientService clientService);
        Task<ResponseModel> DeleteClientService(ClientService clientService, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteClientServiceByClientId(Int32? clientId, Int32? userId, SqlConnection connection, SqlTransaction transaction);       
        Task<List<ClientService>> GetClientServiceByClientId(Int32? clientId);
        Task<List<ClientService>> GetClientServiceByServiceId(Int32? serviceId);
        Task<dynamic> GetClientService(ClientService clientService);
    }
} 
