﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.DAO.Interface
{
    interface IProjectOption
    {
        Task<ResponseModel> InsertComplianceRequirements(ProjectOption projectOption);
        Task<ResponseModel> InsertStorageArchivalRequirements(ProjectOption projectOption);
        Task<ResponseModel> InsertTranslationRequirements(ProjectOption projectOption);
        Task<ResponseModel> UpdateComplianceRequirements(ProjectOption projectOption);
        Task<ResponseModel> UpdateStorageArchivalRequirements(ProjectOption projectOption);
        Task<ResponseModel> UpdateTranslationRequirements(ProjectOption projectOption);
        Task<dynamic> GetProjectOption(ProjectOption projectOption);

    }
}
