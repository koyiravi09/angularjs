﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.DAO.Interface
{
    interface IService
    {
        Task<ResponseModel> InsertService(Service service);
        Task<ResponseModel> UpdateService(Service service);
        Task<ResponseModel> DeleteService(Int32? serviceId,Int32? userId, SqlConnection connection, SqlTransaction transaction);
        Task<List<Service>> GetService(Int32? startIndex, Int32? limit);
        Task<Service> GetService(Int32? serviceId);
        Task<List<Service>> GetService();
        Task<List<dynamic>> GetRoles();
        Task<ResponseModel> InsertRoleService(Role roleService);
        Task<ResponseModel> UpdateRoleService(Role roleService);
        Task<ResponseModel> DeleteRoleService(Role roleService, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteRoleServiceByServiceId(Int32? serviceId,Int32? userId, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteClientServiceByServiceId(Int32? serviceId, Int32? userId, SqlConnection connection, SqlTransaction transaction);
        Task<List<Role>> GetRoleServiceByRoleId(Int32? roleId);
        Task<List<Role>> GetRoleServiceByServiceId(Int32? serviceId);
        Task<dynamic> GetRoleService(Role roleService); 
    }
}
