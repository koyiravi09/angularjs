﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.DAO.Interface
{
    interface IUser
    {
        Task<ResponseModel> InsertUser(RegisterUser user);
        Task<ResponseModel> UpdateUser(RegisterUser user);
        Task<ResponseModel> DeleteUser(Int32? userId, Int32? logInUserId, SqlConnection connection , SqlTransaction transaction );
        Task<List<User>> GetUser(Int32? startIndex, Int32? limit);
        Task<User> GetUser(Int32? userId);
        Task<List<User>> GetUser();
        Task<ResponseModel> InsertClientUser(ClientUser clientUser);
        Task<ResponseModel> UpdateClientUser(ClientUser clientUser);
        Task<ResponseModel> DeleteClientUser(ClientUser clientUser, SqlConnection connection, SqlTransaction transaction);
        Task<dynamic> GetClientUserByClientId(Int32? clientId);
        Task<dynamic> GetClientUserByUserId(Int32? userId);
        Task<dynamic> GetClientUser(ClientUser clientUser);
        Task<ResponseModel> InsertUserRole(UserRole userRole);
        Task<ResponseModel> UpdateUserRole(UserRole userRole);
        Task<ResponseModel> DeleteUserRole(UserRole userRole, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteClientUserByUserId(Int32? userId,Int32? logInUserId, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteClientUserByClientId(Int32? clientId,Int32? logInUserId, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteUserRoleByUserId(Int32? userId, Int32? logInUserId, SqlConnection connection, SqlTransaction transaction);
        Task<dynamic> GetUserRoleByUserId(Int32? userId);
        Task<dynamic> GetUserRoleByRoleIds(string roleIds);
        Task<dynamic> GetUserRoleByRoleId(Int32? roleId);
        Task<dynamic> GetUserRoleByClientServiceRoleId(UserRole userRole);
        Task<dynamic> GetUserRole(UserRole userRole);
        Task<dynamic> GetUserByEmailId(string emailId);
    }
}
