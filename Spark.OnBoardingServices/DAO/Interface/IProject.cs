﻿using Spark.OnBoardingServices.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Spark.OnBoardingServices.DAO.Interface
{
    public interface IProject
    {
        Task<ResponseModel> InsertProject(Project project);
        Task<ResponseModel> UpdateProject(Project project);
        Task<ResponseModel> UpdateProjectStatus(Project project);
        Task<ResponseModel> DeleteProject(Int32? clientProjectId, Int32? userId, SqlConnection connection, SqlTransaction transaction);
        Task<List<Project>> GetProject(Int32? startIndex, Int32? limit);
        Task<dynamic> GetProject(Int32? clientProjectId);
        Task<dynamic> GetProject();
        Task<ResponseModel> InsertProjectService(ProjectService project);
        Task<ResponseModel> UpdateProjectService(ProjectService project);
        Task<ResponseModel> DeleteProjectService(ProjectService project, SqlConnection connection, SqlTransaction transaction);
        Task<List<ProjectService>> GetProjectServiceByProjectId(Int32? clientProjectId);
        Task<dynamic> GetProjectService(ProjectService projectService);
        Task<ResponseModel> DeleteProjectServiceByProjectId(Int32? clientprojectId, Int32? userId, SqlConnection connection, SqlTransaction transaction);
        Task<ResponseModel> DeleteProjectServiceByServiceId(Int32? serviceId, Int32? userId, SqlConnection connection, SqlTransaction transaction);
    }
}
